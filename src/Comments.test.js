import React from 'react'

import {shallow} from 'enzyme'

import Comments from './Comments'

import Comment from './Comment'

describe ('<Comments />' , () => {

    it('should render Comments' , () =>{
        const comments = {
            a:{id:'a', comment : 'teste'},
            b:{id:'b', comment : 'teste2'},
            c:{id:'c', comment : 'teste3'}
        }
        const wrapper = shallow(<Comments comments={comments} />)

        expect(wrapper.find(Comment).length).toBe(3)

        expect(wrapper.find(Comment).get(0).props.c).toBe(comments.a)

        expect(wrapper.find(Comment).get(0).key).toBe(comments.a.id)
    })

    it('should work with no Comments', () => {
        const comments = {}

        const wrapper = shallow(<Comments comments={comments}/>)

        expect(wrapper.find(Comment).length).toBe(0)
    })

})