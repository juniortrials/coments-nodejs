import React, { Component } from "react";

class SignUp extends Component {
  state = {
    email: "",
    passwd: ""
  };

  handleChange = field => event => {
    this.setState({
      [field]: event.target.value
    });
  };

  createAccount = () => {
    this.props.createAccount(this.state.email, this.state.passwd);
  };

  render() {
    const errorMessages = {
      "auth/wrong-password": "Email ou senha incorretas",
      "auth/user-not-found": "Usuário não encontrado",
      "auth/invalid-email": "Email inválido"
    };
    return (
      <div>
        <h4>Criar Conta</h4>
        <input
          type="text"
          onChange={this.handleChange("email")}
          placeholder="email"
        />
        <input
          type="password"
          onChange={this.handleChange("passwd")}
          placeholder="senha"
        />
        <button type="button" onClick={this.createAccount}>
          Logar
        </button>
        {this.props.isAuthError && (
          <p>
            <b>{errorMessages[this.props.authError]}</b>
          </p>
        )}
      </div>
    );
  }
}

export default SignUp;
