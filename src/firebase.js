import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyAvv2MOVlvJjZkTiFp5TGj4MX-1H83d2v0",
  authDomain: "comments-devreactjs22.firebaseapp.com",
  databaseURL: "https://comments-devreactjs22.firebaseio.com",
  projectId: "comments-devreactjs22",
  storageBucket: "comments-devreactjs22.appspot.com",
  messagingSenderId: "488614102131"
};

firebase.initializeApp(config);

export const database = firebase.database();

export const auth = firebase.auth();
