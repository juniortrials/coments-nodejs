import React, { Component } from "react";

class Login extends Component {
  state = {
    email: "",
    passwd: ""
  };

  handleChange = field => event => {
    this.setState({
      [field]: event.target.value
    });
  };

  login = () => {
    this.props.login(this.state.email, this.state.passwd);
  };

  render() {
    const errorMessages = {
      "auth/wrong-password": "Email ou senha incorretas",
      "auth/user-not-found": "Usuário não encontrado",
      "auth/invalid-email": "Email inválido"
    };
    return (
      <div>
        <h4>Login</h4>
        <input
          type="text"
          onChange={this.handleChange("email")}
          placeholder="email"
        />
        <input
          type="password"
          onChange={this.handleChange("passwd")}
          placeholder="senha"
        />
        <button type="button" onClick={this.login}>
          Logar
        </button>
        {this.props.isAuthError && (
          <p>
            <b>{errorMessages[this.props.authError]}</b>
          </p>
        )}
        <button onClick={() => this.props.changeScreen("signup")}>
          Criar Conta
        </button>
      </div>
    );
  }
}

export default Login;
