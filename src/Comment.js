import React from "react";

const Comment = ({ c }) => {
  let comment = "vazio";
  let email = null;
  if (c) {
    if (c.comment) {
      comment = c.comment;
    }
    if (c.email) {
      email = c.email;
    }
  }
  return (
    <div>
      {comment}
      <br />
      {email && <b>Enviado por : {email} </b>}
      <hr />
    </div>
  );
};

export default Comment;
